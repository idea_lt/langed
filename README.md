SilverStripe 3 Language Editor Module
========================================

Maintainer Contacts
-------------------
*  IDEA (<silverstripe@idea.lt>)

Requirements
------------
* SilverStripe 3.x
* Translatable (module)

Installation Instructions
-------------------------

1. Place this directory in the root of your SilverStripe installation.
2. Visit `project/dev/build` to rebuild the database.

Usage Overview
--------------
List modules you need to translate in `mysite/_config.php`
Ex. `TranslationCollector::setCollectableModules(array('faq', 'news', 'themes/mytheme'));`

### Add these lines into your Page.php file Page_Controller::init method
`i18n::set_locale(Translatable::get_current_locale());`
`i18n::set_default_locale(Translatable::get_current_locale());`

### Join all listed translations into one file
1. Run `http://project/dev/tasks/i18nTextCollectorTask?locale=en_US`
2. Now run collector task for `langed` module `http://project/dev/tasks/i18nTextCollectorTask?locale=en_US&module=langed`
3. After this you have `langed/lang/en_US.yml` which has all the translations from the modules listed in `mysite/_config.php`
4. You can translate it in admin. "Language Editor" tab.
5. For other locales repeat 1-4 steps, just replace en_US with another locale.

Known Issues
------------
[Issue Tracker](https://bitbucket.org/idea_lt/langed/issues?status=new&status=open)
