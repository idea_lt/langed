(function($) {
	$.entwine('ss', function($){
		$('#Form_EditForm_Locale').entwine({
			onchange: function(e) {
				this.removeClass('changed');
				var locationNoParams = window.location.href.split('?')[0];
				$('.cms-container').loadPanel(locationNoParams + '?locale=' + this.val());
				this._super(e);
			}
		});
	});
}(jQuery));
